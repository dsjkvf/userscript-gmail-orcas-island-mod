// ==UserScript==
// @name            Google Mail - Orcas Island Mod Theme
// @description     Modifies the Orcastheme
// @version         0.91
// @namespace       oyvey
// @homepage        https://bitbucket.org/dsjkvf/gmail-orcas-island-mod
// @downloadURL     https://bitbucket.org/dsjkvf/gmail-orcas-island-mod/raw/master/orcas.js
// @updateURL       https://bitbucket.org/dsjkvf/gmail-orcas-island-mod/raw/master/orcas.js
// @include         http*://mail.google.com/*
// @match           http://mail.google.com/*
// @match           https://mail.google.com/*
// @grant           GM_addStyle
// @grant           GM_registerMenuCommand
// ==/UserScript==

var cssMon = `
.wl{background:#738996;overflow:hidden}
.ao0{left:0;position:absolute}
.a4t{width:100%;height:100%;background-size:cover;background-position:center}
.ao1{-moz-transform:scaleX(-1);transform:scaleX(-1);filter:FlipH}
.cS .wp{background:url(http://i.imgur.com/XI5CfTn.jpg) no-repeat top left}
.cN .wp{background:url(http://i.imgur.com/XI5CfTn.jpg) no-repeat top left}
.cL .wp{background:url(http://i.imgur.com/XI5CfTn.jpg) no-repeat top left}
.cR .wp{background:url(http://i.imgur.com/XI5CfTn.jpg) no-repeat top left}
`;
var cssTue = `
.wl{background:#5e7056;overflow:hidden}
.ao0{left:0;position:absolute}
.a4t{width:100%;height:100%;background-size:cover;background-position:center}
.ao1{-moz-transform:scaleX(-1);transform:scaleX(-1);filter:FlipH}
.cS .wp{background:url(http://i.imgur.com/Vgp6Qgr.jpg) no-repeat top left}
.cN .wp{background:url(http://i.imgur.com/Vgp6Qgr.jpg) no-repeat top left}
.cL .wp{background:url(http://i.imgur.com/Vgp6Qgr.jpg) no-repeat top left}
.cR .wp{background:url(http://i.imgur.com/Vgp6Qgr.jpg) no-repeat top left}
`;
var cssWed = `
.wl{background:#4d3630;overflow:hidden}
.ao0{left:0;position:absolute}
.a4t{width:100%;height:100%;background-size:cover;background-position:center}
.ao1{-moz-transform:scaleX(-1);transform:scaleX(-1);filter:FlipH}
.cS .wp{background:url(http://i.imgur.com/jRj95fi.jpg) no-repeat top left}
.cN .wp{background:url(http://i.imgur.com/jRj95fi.jpg) no-repeat top left}
.cL .wp{background:url(http://i.imgur.com/jRj95fi.jpg) no-repeat top left}
.cR .wp{background:url(http://i.imgur.com/jRj95fi.jpg) no-repeat top left}
`;
var cssThu = `
.wl{background:#6e6965;overflow:hidden}
.ao0{left:0;position:absolute}
.a4t{width:100%;height:100%;background-size:cover;background-position:center}
.ao1{-moz-transform:scaleX(-1);transform:scaleX(-1);filter:FlipH}
.cS .wp{background:url(http://i.imgur.com/oAJE0C8.jpg) no-repeat top left}
.cN .wp{background:url(http://i.imgur.com/oAJE0C8.jpg) no-repeat top left}
.cL .wp{background:url(http://i.imgur.com/oAJE0C8.jpg) no-repeat top left}
.cR .wp{background:url(http://i.imgur.com/oAJE0C8.jpg) no-repeat top left}
`;
var cssFri = `
.wl{background:#738996;overflow:hidden}
.ao0{left:0;position:absolute}
.a4t{width:100%;height:100%;background-size:cover;background-position:center}
.ao1{-moz-transform:scaleX(-1);transform:scaleX(-1);filter:FlipH}
.cS .wp{background:url(http://i.imgur.com/dXeOMkS.jpg) no-repeat top left}
.cN .wp{background:url(http://i.imgur.com/dXeOMkS.jpg) no-repeat top left}
.cL .wp{background:url(http://i.imgur.com/dXeOMkS.jpg) no-repeat top left}
.cR .wp{background:url(http://i.imgur.com/dXeOMkS.jpg) no-repeat top left}
`;
var cssSat = `
.wl{background:#738996;overflow:hidden}
.ao0{left:0;position:absolute}
.a4t{width:100%;height:100%;background-size:cover;background-position:center}
.ao1{-moz-transform:scaleX(-1);transform:scaleX(-1);filter:FlipH}
.cS .wp{background:url(http://i.imgur.com/hruh8F3.jpg) no-repeat top left}
.cN .wp{background:url(http://i.imgur.com/hruh8F3.jpg) no-repeat top left}
.cL .wp{background:url(http://i.imgur.com/hruh8F3.jpg) no-repeat top left}
.cR .wp{background:url(http://i.imgur.com/hruh8F3.jpg) no-repeat top left}
`;
var cssSun = `
.wl{background:#6e6965;overflow:hidden}
.ao0{left:0;position:absolute}
.a4t{width:100%;height:100%;background-size:cover;background-position:center}
.ao1{-moz-transform:scaleX(-1);transform:scaleX(-1);filter:FlipH}
.cS .wp{background:url(http://i.imgur.com/hQjMWG9.jpg) no-repeat top left}
.cN .wp{background:url(http://i.imgur.com/hQjMWG9.jpg) no-repeat top left}
.cL .wp{background:url(http://i.imgur.com/hQjMWG9.jpg) no-repeat top left}
.cR .wp{background:url(http://i.imgur.com/hQjMWG9.jpg) no-repeat top left}
`;

function applyMyCSS1() {
    GM_addStyle(cssMon);
}
function applyMyCSS2() {
    GM_addStyle(cssTue);
}
function applyMyCSS3() {
    GM_addStyle(cssWed);
}
function applyMyCSS4() {
    GM_addStyle(cssThu);
}
function applyMyCSS5() {
    GM_addStyle(cssFri);
}
function applyMyCSS6() {
    GM_addStyle(cssSat);
}
function applyMyCSS7() {
    GM_addStyle(cssSun);
}

GM_registerMenuCommand("Gmail Orcas Theme: Apply Monday CSS", applyMyCSS1, "1");
GM_registerMenuCommand("Gmail Orcas Theme: Apply Tuesday CSS", applyMyCSS2, "2");
GM_registerMenuCommand("Gmail Orcas Theme: Apply Wednesday CSS", applyMyCSS3, "3");
GM_registerMenuCommand("Gmail Orcas Theme: Apply Thursday CSS", applyMyCSS4, "4");
GM_registerMenuCommand("Gmail Orcas Theme: Apply Friday CSS", applyMyCSS5, "5");
GM_registerMenuCommand("Gmail Orcas Theme: Apply Saturday CSS", applyMyCSS6, "6");
GM_registerMenuCommand("Gmail Orcas Theme: Apply Sunday CSS", applyMyCSS7, "7");

setTimeout(function(){applyMyCSS1();}, 30000);

// define key listener
document.addEventListener("keydown",keyStart,false);
    function keyStart(event) {
        if ((event.altKey == 0) && ((event.shiftKey == 0) && (event.ctrlKey == 1))) {
            if (event.keyCode==65) {
                applyMyCSS1();
            }
            else if (event.keyCode == 66) {
                applyMyCSS2();
            }
            else if (event.keyCode == 67) {
                applyMyCSS5();
            }
        }
    }
