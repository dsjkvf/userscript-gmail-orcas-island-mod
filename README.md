Orcas Island Gmail Theme Modification
=====================================

## About

This is a [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which introduces commands to select Orcas Island Gmail subthemes irrespectively of an actual weekday.

## Usage

Install the userscript, open Gmail page (reload if needed), and then open Menu -> Tools -> Greasemonkey -> User Script Commands.
